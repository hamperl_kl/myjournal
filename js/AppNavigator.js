import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';

import {SimpleLineIcons} from '@expo/vector-icons';

import JournalScreen from './screens/JournalScreen';
import PhotosScreen from './screens/PhotosScreen';
import SettingsScreen from './screens/SettingsScreen';
import ItemScreen from './screens/ItemScreen';
import EditScreen from './screens/EditScreen';

import {Platform, StatusBar, View} from 'react-native';
import TouchableItem from './components/TouchableItem';

const Tabs = createBottomTabNavigator(
    {
        Journal: {
            screen: JournalScreen,
            navigationOptions: {
                title: 'Tagebuch',
                tabBarIcon: ({tintColor}) => <SimpleLineIcons name={'book-open'} size={24} color={tintColor}/>
            }
        },
        Photos: {
            screen: PhotosScreen,
            navigationOptions: {
                title: 'Fotos',
                tabBarIcon: ({tintColor}) => <SimpleLineIcons name={'picture'} size={24} color={tintColor}/>
            }
        },
        Settings: {
            screen: SettingsScreen,
            navigationOptions: {
                title: 'Einstellungen',
                tabBarIcon: ({tintColor}) => <SimpleLineIcons name={'settings'} size={24} color={tintColor}/>
            }
        }
    },
    {
        tabBarPosition: 'bottom',
        tabBarOptions: {
            activeTintColor: '#5b9cd5',
            inactiveTintColor: '#929292',
            style: {
                backgroundColor: '#f4f4f4',
            },
            indicatorStyle: {
                height: 0,
            },
            showIcon: true,
            upperCaseLabel: false,
            labelStyle: {
                ...Platform.select({
                    android: {
                        marginBottom: 0
                    }
                })
            }
        }
    }
);
const AppNavigator = createStackNavigator(
    {
        Root: {
            screen: Tabs,
            navigationOptions: ({navigation}) => ({
                title: 'Tagebuch',
                headerRight: (
                    <TouchableItem onPress={() => {
                        const newItem = {text: null, photo: null, date: null};
                        navigation.navigate('Edit', {item: newItem});
                    }}>
                        <View>
                            <SimpleLineIcons style={{padding: 10}} name={'plus'} size={24} color={'#5b9cd5'}/>
                        </View>
                    </TouchableItem>
                )
            })
        },
        Item: {
            screen: ItemScreen,
        },
        Edit: {
            screen: EditScreen,
        }
    },
    {
        navigationOptions: {
            headerTintColor: '#5b9cd9',
            headerStyle: {
                ...Platform.select({
                    android: {marginTop: StatusBar.currentHeight},
                    ios: {backgoundColor: 'white'}
                })
            }
        },
        cardStyle: {
            backgroundColor: 'white'
        }
    }
);


export default createAppContainer(AppNavigator);