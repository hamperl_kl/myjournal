import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    SectionList,
} from 'react-native';
import JournalItemRow from './JournalItemRow';

class JournalItems extends Component {
    render() {
        if (this.props.items.length === 0) {
            return (
                <View style={styles.noItems}>
                    <Text style={styles.infoText}>Keine Einträge im Tagebuch</Text>
                </View>
            )
        }
        return (
            <SectionList sections={this.props.items}
                         renderItem={({item}) => (
                            <JournalItemRow item={item} onPress={() => this.props.onPress(item)}/>
                         )}
                         renderSectionHeader={({section}) => (<Text style={styles.listHeader}>{section.title}</Text>)}
                         keyExtractor={item => item.date.toString()}
                         ItemSeparatorComponent={() => <View style={styles.listSeparator}/>}
            />
        );
    }
}

const styles = StyleSheet.create({
    noItems: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    listHeader: {
        backgroundColor: '#5b9cd5',
        textAlign: 'center',
        color: '#FFF'
    },
    infoText: {
        color: 'darkslategray',
        fontSize: 22,
        fontWeight: '300'
    },
    listSeparator: {
        height: 1,
        backgroundColor: 'lightblue'
    }
});


export default JournalItems ;