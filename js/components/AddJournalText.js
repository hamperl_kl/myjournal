import React, {Component} from 'react';
import {
    StyleSheet,
    TextInput,
    KeyboardAvoidingView,
    View,
} from 'react-native';

class AddJournalText extends Component {


    render() {
        console.log(this.props)
        return (
            <KeyboardAvoidingView behavior={"padding"}>
                <View style={styles.inputContainer}>
                    <TextInput style={styles.input}
                               ref={this.props.theRef}
                               placeholder={this.props.placeholder}
                               underlineColorAndroid={'transparent'}
                               returnKeyType={'done'}
                               onSubmitEditing={event => {this.props.addItem(event.nativeEvent.text)}}
                    />
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    inputContainer: {
        borderColor: '#5b9cd5',
        borderRadius: 8,
        borderWidth: 1,
        marginTop: 5,
        marginBottom: 25,
        marginHorizontal: 10,
        paddingHorizontal: 10
    },
    input: {
        height: 40
    }
});

export default AddJournalText;