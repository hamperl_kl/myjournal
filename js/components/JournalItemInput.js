import React, {Component} from 'react';
import {
    StyleSheet,
    TextInput,
    Image,
    KeyboardAvoidingView,
    View,
    Alert,
} from 'react-native';
import {SimpleLineIcons} from '@expo/vector-icons';
import TouchableIcon from './TouchableItem';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
import Store from '../Store'

class JournalItemInput extends Component {
    state = {
        photo: null
    };

    _deleteItems() {
      Alert.alert('Einträge Löschen?', 'Sollen wircklich alle einträge gelöscht werden?',
          [
            {
              text: 'Nein',
              style: 'cancel'
            },
              {
                  text: 'Ja',
                  onPress: async () => {await Store.deleteItems(); this.props.refresh()}
              }
          ]
          )
    };
    _getWeather = async () => {
        let result = {location: null, weather: null};
        try {
            const {status} = await Permissions.askAsync(Permissions.LOCATION);
            if (status !== 'granted') {
                console.log('Permissions to access location was denied');
                return result;
            }

        const position = await Location.getCurrentPositionAsync({});
        const {longitude, latitude} = position.coords;
        const location = `lon=${longitude}&lat=${latitude}`;
        const apiKey = 'appid=083880e08076d1e5ce4701657f47df52';
        const url = 'http://api.openweathermap.org/data/2.5/weather?' +
            location +
            '&' + apiKey +
            '&units=metric&lang=de';
            const response = await fetch(url);
            const weatherJSON = await response.json();
            const {weather, main, name} = weatherJSON;
            result = {location: name, weather: `${Math.floor(main.temp)}°C ${weather[0].description}`};
        } catch (error) {
            console.log('Error fetching Weather! ', error)
        }

        return result
    };
    _submitWeather = async (text, photo) => {
        const {location, weather} = await this._getWeather();
        this.props.onSubmit({text, photo, location, weather});
    };
    _submitText(text) {
        this.textInput.clear();
        this._submitWeather(text, this.state.photo);
        this.setState({photo: null});
    }

    _hasCameraPermissions = async () => {
        let permission = await Permissions.askAsync(Permissions.CAMERA);
        if (permission.status !== 'granted') {
            console.log('Permission to camera was denied');
            return false;
        }
        permission = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (permission.status !== 'granted') {
            console.log('Permission to camera roll was denied');
            return false;
        }
        return true;
    };


    _launchCamera = async () => {
        if (this._hasCameraPermissions()) {
            const result = await ImagePicker.launchCameraAsync();
            if (!result.cancelled) {
                this.setState({ photo: result.uri });
                this.textInput.focus();
            }
        }
    }
    render() {
        const photoIcon = this.state.photo
            ? <Image style={styles.imgPreview} source={{uri: this.state.photo}}/>
            : <SimpleLineIcons name={'camera'} size={24} color={'#5b9cd5'}/>
        return (
            <KeyboardAvoidingView behavior={"padding"} keyboardVerticalOffset={160}>
                <View style={styles.container}>
                <View style={styles.inputContainer}>
                    <View style={styles.photoIcon}>
                    <TouchableIcon onPress={() => this._launchCamera()}>
                        {photoIcon}
                    </TouchableIcon>
                    </View>
                    <TextInput style={styles.input}
                               ref={input => (this.textInput = input)}
                               placeholder={this.props.placeholder}
                               underlineColorAndroid={'transparent'}
                               returnKeyType={'done'}
                               onSubmitEditing={event => {this._submitText(event.nativeEvent.text)}}
                    />
                </View>
                <TouchableIcon onPress={() => this._deleteItems()}>
                    <View>
                        <SimpleLineIcons name={'trash'} size={24} color={'#5b9cd5'}/>
                    </View>
                </TouchableIcon>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
      alignItems: 'center'
    },
    inputContainer: {
        flex: 1,
        flexDirection: 'row',
        borderColor: '#5b9cd5',
        borderRadius: 8,
        borderWidth: 1,
        marginTop: 5,
        marginBottom: 25,
        marginHorizontal: 10,
        paddingHorizontal: 10
    },
    photoIcon: {
      alignSelf: 'center',
      marginLeft: 5,
      marginRight: 15,
    },
    input: {
        flex: 1,
        height: 40
    },
    imgPreview: {
        width: 24,
        height: 24
    }
});

export default JournalItemInput;