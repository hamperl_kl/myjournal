import React, {Component} from 'react';
import {Image, StyleSheet, TextInput, View} from 'react-native';

import {SimpleLineIcons} from '@expo/vector-icons';

import TouchableItem from '../components/TouchableItem';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';

export default class EditScreen extends Component {
    state = {
        item: this.props.navigation.state.params.item
    };

    _hasCameraPermissions = async () => {
        let permission = await Permissions.askAsync(Permissions.CAMERA);
        if (permission.status !== 'granted') {
            console.log('Permission to camera was denied');
            return false;
        }
        permission = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (permission.status !== 'granted') {
            console.log('Permission to camera roll was denied');
            return false;
        }
        return true;
    };

    _launchCamera = async () => {
        if (this._hasCameraPermissions()) {
            const result = await ImagePicker.launchCameraAsync();
            if (!result.cancelled) {
                const {item} = this.state;
                item.photo = result.uri;
                this.setState({item: item});
            }
            this.textInput.focus();
        }
    };

    _getWeather = async item => {
        try {
            const {status} = await Permissions.askAsync(Permissions.LOCATION);
            if (status !== 'granted') {
                console.log('Permissions to access location was denied');
                return null;
            }

            const position = await Location.getCurrentPositionAsync({});
            const {longitude, latitude} = position.coords;
            const location = `lon=${longitude}&lat=${latitude}`;
            const apiKey = 'appid=083880e08076d1e5ce4701657f47df52';
            const url = 'http://api.openweathermap.org/data/2.5/weather?' +
                location +
                '&' + apiKey +
                '&units=metric&lang=de';
            const response = await fetch(url);
            const weatherJSON = await response.json();
            const {weather, main, name} = weatherJSON;
            item.location = name;
            item.weather = `${Math.floor(main.temp)}°C ${weather[0].description}`;
            this.setState({item: item});
        } catch (error) {
            console.log('Error fetching Weather! ', error)
        }
    };

    componentWillUnmount() {
        this.props.screenProps.onSubmit(this.state.item);
    };

    componentWillMount() {
        const {item} = this.state;
        if (item.date === null) this._getWeather(item);
    }

    render() {
        const {item} = this.state;
        const photoIcon = item.photo ? (
            <Image style={styles.imagePreview} source={{uri: item.photo}}/>
        ) : (
            <SimpleLineIcons name={"camera"} size={48} color={'#5b9cd5'}/>
        );

        return (
           <View style={styles.container}>
               <TextInput style={styles.input}
                          ref={input => (this.textInput = input)}
                          autoFocus={true}
                          multiline={true}
                          underlineColorAndroid={"transparent"}
                          onChangeText={text => {
                              item.text = text;
                              this.setState({item: item})
                          }}
                          value={item.text}
               />
               <View style={styles.photoIcon}>
                   <TouchableItem onPress={() => this._launchCamera()}>
                       {photoIcon}
                   </TouchableItem>
               </View>
           </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    input: {
        fontSize: 16,
        height: '40%',
        textAlignVertical: 'top'
    },
    photoIcons: {
        alignSelf: 'center',
        marginLeft: 5,
        marginRight: 15
    },
    imagePreview: {
        width: 48,
        height: 48
    }
})
